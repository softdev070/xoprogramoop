/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wimonsiri.oxprogramoop;

/**
 *
 * @author Acer
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'},};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean Finish = false;
    private int lastcol;
    private int lastrow;
    private int turn = 0;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer =   x;
    }

    public void showTable() {
        System.out.println("  1  2  3 ");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(" " + table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }

    public Player getcurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkcol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setstatWinLose();
    }

    private void setstatWinLose() {
        if (currentPlayer == playerO) {
            playerO.Win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.Win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
    }

    void checkX_Left() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (row == col && table[row][col] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        Finish = true;
        winner = currentPlayer;
        setstatWinLose();
    }

    void checkX_Right() {
        int col = 2;
        int row = 0;
        for (int round = 0; round < 3; round++) {
            if (table[row][col] != currentPlayer.getName()) {
                return;
            }
            col--;
            row++;
        }
        Finish = true;
        winner = currentPlayer;
    }

    void checkDraw() {
        playerO.draw();
        playerX.draw();
        }
 
    

    public void checkWin() {
        checkcol();
        checkRow();
        checkX_Left();
        checkX_Right();
        checkDraw();
    }

    public boolean isFinish() {
        return Finish;
    }

    public Player getWinner() {
        return winner;
    }
}
